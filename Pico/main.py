from display.max7219 import SevenSegment
from datamanager import DataManager
from mqtt import CCCMQTTClient
from network import STA_IF, WLAN
from utime import sleep
from machine import reset

from gc import collect
collect()


def main():
    # Create a client object
    client = Client(r'login.json', r'schedule.txt',
                    {"login": b"login", "assignments": b"assignments"}, b"clocks")
    
    # Start the loop
    client.loop()

class Client():

    def __init__(self, loginFile, scheduleFile, subTopics, pubTopic):
        """Run the client node."""
        self.display = SevenSegment()

        # Get the data stored in the files
        print('Getting Data')
        self.display.text('LoAd')
        self.data = DataManager(loginFile, scheduleFile, subTopics, pubTopic)

        # Connect to the Internet
        print('Connecting to Internet')
        self.display.text('IntErnEt')
        self.wifi_connect()

        # Connect to the MQTT Broker
        print('Connecting to MQTT Broker:', self.data.client_id)
        self.display_text(self.data.decode_text(self.data.client_id))
        self.mqtt = CCCMQTTClient(self.data.client_id, self.data.mqtt)
        self.mqtt.set_callback(self.main_cb)
        self.mqtt.setup_connect(self.data.get_subs(), self.data.pub) 
        

    def wifi_connect(self):
        """Connect to the internet by iterating through the available options."""

        wlan=WLAN(STA_IF)
        wlan.active(True)
        if not wlan.isconnected():
            print('connecting to network...')
            while not wlan.isconnected():
                wlan.disconnect()
                wlan.connect(*self.data.get_wifi())
                sleep(5)
        print('network config:', wlan.ifconfig())

    def wifi_disconnect(self):
        """Disconnect from the internet."""

        wlan=WLAN(STA_IF)
        wlan.active(True)
        wlan.disconnect()
        print("Disconnected from network.")

    def display_text(self, text):
        """Determine how text should be displayed."""

        if text != 'off':
            self.display.text(text)
            print('Displayed:', text)

        # If the text is off, then clear the display
        else:
            self.display.clear(True)

    def main_cb(self, topic, msg, retained, duplicate):
        """Receive all callback, and reroute them to specific callbacks."""
        
        # For testing, print the callback
        self.print_cb(topic, msg, retained, duplicate)

        if topic == self.data.schedule:
            self.schedule_cb(msg, retained, duplicate)

        elif topic == self.data.login:
            self.login_cb(topic, msg, retained, duplicate)

        elif topic == self.data.assignments:
            self.assignments_cb(topic, msg, retained, duplicate)

    def schedule_cb(self, msg, retained, duplicate):
        """Callback for the schedule. Display the message on the 8 digit 7 segment display."""

        # Decode the ascii
        message=self.data.decode_text(msg)

        # Display the message on the display
        self.display_text(message)
    
    def login_cb(self, topic, msg, retained, duplicate):
        """Callback for updates to the login data. Reboot if the login data changed."""
        
        rebootNeeded = self.data.update_login(msg)
        
        if rebootNeeded:
            self.reboot(topic)
    
    def assignments_cb(self, topic, msg, retained, duplicate):
        """Callback for updates to the schedule assignments for the clocks. Reboot if the schedule changed."""
        
        rebootNeeded = self.data.update_schedule(msg)
        
        if rebootNeeded:
            self.reboot(topic)

    def print_cb(self, topic, msg, retained, duplicate):
        """Print callbacks for testing."""
        print('Callback:')
        print((topic, msg, retained, duplicate))

    def loop(self):
        """Main loop that sends data to and receives data from the broker."""

        print('Started receiving...')
        self.display_text('StArt')
        while True:
            sleep(0.1)
            if self.mqtt.is_conn_issue():
                for _ in range(100):
                    if not(self.mqtt.is_conn_issue()): break
                    else:
                        self.display_text('reconnectInG')
                        self.mqtt.reconnect()
                        
                else: self.reboot(b'reconnectInG') # Reboot if reconnection was unsuccessful
                
                # Resubscribe if reconnection was successful
                self.display_text('reSUbScrIbInG')
                self.mqtt.resubscribe()

            # Check if there are any messages
            self.mqtt.check_msg()
            self.mqtt.send_queue()

    def reboot(self, reason:bytes):
        """Reboot the clock to implement changes."""
        
        self.display_text(f"reboot.%s" % self.data.decode_text(reason))
        print('Rebooting:', reason)
        
        # Publish the client id to a reboot topic
        topic = b'%s/reboot/%s' % (self.data.pub, reason)
        self.mqtt.publish(topic, self.data.client_id)
        
        sleep(2)
        self.disconnect()
        
        # Perform a hard reset
        reset()
        
    def disconnect(self):
        """Disconnect from the mqtt broker and WiFi."""
        self.mqtt.disconnect()
        self.wifi_disconnect()

main()

