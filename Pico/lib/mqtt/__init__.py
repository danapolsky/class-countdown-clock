from .robust2 import MQTTClient

# Import getaddrinfo from socket to get the ip address from the host address
from usocket import getaddrinfo

class CCCMQTTClient(MQTTClient):

    def __init__(self, id: bytes, mqtt_data: dict):
        """MQTTClient subclass for receiving and sending data to the CCC server."""

        super().__init__(id, self.get_ip_addr(mqtt_data['host_address']),
                         user=mqtt_data['user'], password=mqtt_data['password'])

        # Change the settings of the MQTT client
        self.DEBUG = True
        self.KEEP_QOS0 = False
        self.NO_QUEUE_DUPS = True
        self.MSG_QUEUE_MAX = 2
    
    def get_ip_addr(self, hostAddr:str, port=1883) -> str:
        """Get the ip address of the broker using its local host address."""
        return getaddrinfo(hostAddr, port)[0][-1][0]

    def setup_connect(self, subs: list, pub: str):
        """Connect to the broker and set up a new session."""

        if not self.connect(clean_session=True):
            print("New session being set up.")
            
            # Subscribe to the topics
            [self.subscribe(topic) for topic in subs]
            # Publish the node's id to the publish/startup topic
            self.publish(pub + b'/startup', self.client_id)