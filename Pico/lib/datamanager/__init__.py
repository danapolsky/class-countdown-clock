# To get and edit the JSON data
from ujson import load, dump, loads

# To get the client ID
from machine import unique_id
from ubinascii import hexlify


class DataManager():
    """Get and edit the WiFi, MQTT login, MQTT schedule, the client ID, and the topics."""

    def __init__(self, loginFile: str, scheduleFile: str, sub_topics: dict, pubTopic: bytes):
        self.__loginFile = loginFile
        self.__scheduleFile = scheduleFile
        self.__sub_topics = sub_topics
        self.__pubTopic = pubTopic
        
        # Set the encoding
        self.ENCODING = "ascii"
        self.encode_text = lambda text: bytes(text, self.ENCODING)
        self.decode_text = lambda text: text.decode(self.ENCODING)

        # Get the data
        self.__load()

        # Get the unique ID
        self.__client_id = hexlify(unique_id())

        # Set up for WiFi
        self.__wifi_codes = self.__login_data['wifi']
        self.__wifi_keys = list(self.__wifi_codes.keys())
        self.__wifi_index = 0
        
        print("login:", self.__login_data)
        print("Schedule:", self.__schedule_data)

    def __load(self):
        """Get the data from file."""
        with open(self.__loginFile, 'r') as fileObject:
            self.__login_data = load(fileObject)

        with open(self.__scheduleFile, 'rb') as fileObject:
            self.__schedule_data = fileObject.read()

    def __dump_login(self):
        """Write the login data to its file."""
        with open(self.__loginFile, 'w') as fileObject:
            dump(self.__login_data, fileObject)

    def __dump_schedule(self):
        """Write the schedule data to its file."""
        with open(self.__scheduleFile, 'wb') as fileObject:
            fileObject.write(self.__schedule_data)

    def update_login(self, receivedData: bytes):
        """Update the login data and its file if new data has been received. Returns True if the login data has been updated, returns False otherwise."""
        
        # Try to load the JSON data
        try:
            new_data = loads(receivedData)
        
        except ValueError:
            return False
        
        else:
            
            # If new login data was received, then update the file and return True
            if new_data != self.__login_data:
                self.__login_data.update(new_data)
                self.__dump_login()
                return True
            
            else:
                return False

    def update_schedule(self, receivedData: bytes):
        """Update the schedule data and its file if new data has been received.
        Returns True if the schedule data has been updated or cannot be found, returns False otherwise."""

        # Try to load the JSON data and find the new schedule value under the id key
        try:
            new_assignments_dict = loads(receivedData)
            decoded_id = self.decode_text(self.client_id)
            newSchedule = new_assignments_dict[decoded_id]
            
        except ValueError:
            return False
        
        # If the id is not in the dictionary, reboot in order to let the server know
        except KeyError:
            return True
        
        else:
            
            # Encode the schedule
            newSchedule = self.encode_text(newSchedule)
            
            # If the new schedule does not match the current schedule, then update the file and return True
            if newSchedule != self.__schedule_data:
                self.__schedule_data = newSchedule
                self.__dump_schedule()
                return True
            
            else:
                return False

    def get_wifi(self) -> tuple:
        """Get the username and password."""
        try:
            essid = self.__wifi_keys[self.__wifi_index]
        except IndexError:
            self.__wifi_index = 0
            return self.get_wifi()
        else:
            self.__wifi_index += 1
            password = self.__wifi_codes[essid]
            return essid, password

    @property
    def schedule(self):
        return self.__schedule_data
    
    @property
    def login(self):
        return self.__sub_topics["login"]
    
    @property
    def assignments(self):
        return self.__sub_topics["assignments"]
    
    @property
    def pub(self):
        return self.__pubTopic

    @property
    def mqtt(self):
        return self.__login_data['mqtt']
    
    @property
    def client_id(self):
        return self.__client_id

    def get_subs(self):
        """Get the topics to subscribe to."""
        topics = list(self.__sub_topics.values())
        topics.append(self.__schedule_data)
        return topics
